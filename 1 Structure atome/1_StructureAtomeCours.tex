\documentclass[a4paper,11pt,twoside,french]{article}

\input{../../../mes docs LaTeX/mescommandes.tex}
\input{../../preambuleBTSABM.tex}
\usepackage{bohr}
\usepackage{standalone}
\usepackage{pgf-spectra}
\usepackage{pgfplotstable}
\usepackage{wrapstuff}
\usepackage[prof]{optional}

% Commmande affichant les cases quantiques.
% L'argument est 0, 1 , ou 2 selon le nombre de flèches.
\newcommand*{\caseq}[1]{%
	\ifnum#1=0%
	\fbox{%
		\phantom{$\uparrow$~$\downarrow$}%
	}\xspace
	\else
	\ifnum#1=1%
	\fbox{%
		$\uparrow$\phantom{~$\downarrow$}%
	}\xspace
	\else
	\fbox{%
		$\uparrow$~$\downarrow$%
	}\xspace
	\fi
	\fi
	\hspace{-0.7em}
}

\setlength{\cftparskip}{-0.5ex}% espace entre lignes (défaut : 0)
%------------------------------------------TITRE

\newcommand\titre{Ch.1 Structure de l'atome}
\newcommand{\domaine}{Chimie}
% --------------------------STYLE DE PAGE

\usepackage{fancyhdr}	%en tete et pied de pages
\fancypagestyle{monstyle} % le nom que je lui donne
{
	\fancyhead[L]{\texttt{BTS ABM}}
	\fancyhead[R]{\domaine~\textsc{\titre}}
	\fancyfoot{}
	\fancyfoot[LE,RO]{\thepage ~$\star$ \pageref{LastPage} }
	\renewcommand{\headrulewidth}{1pt} % Séparateur en-tête
	\renewcommand{\footrulewidth}{1pt} % Séparateur pied
}
% --------------------------------------------------------------

%\renewcommand\cftsecnumwidth{0.5cm}% règle la place pour le titre de la section dans la TOC
\renewcommand{\thesection}{\Roman{section}} % Pour numéroter en chiffres romains les sections (paragraphes)

%\setcounter{tocdepth}{2}% profondeur des niveaux de la table

\tikzset{every picture/.style={line cap = butt}}

\setlength{\cftbeforetoctitleskip}{3ex plus 0.3ex minus 0.2ex}%espace avant le titre de la toc (permet de le remonter)
\setlength{\cftaftertoctitleskip}{5ex plus 0.3ex minus 0.2ex}

\renewcommand{\cfttoctitlefont}{\Large\sffamily}

\begin{document}


	\pagestyle{monstyle}
	\renewcommand{\labelitemi}{\textbullet}
	\renewcommand\labelitemii{$\square$}

	%--------------réglages personnalisés

	\setlength{\parindent}{0em} %indentation 1e ligne du paragraphe
	\setlength{\parskip}{0.5em} %intervalle entre paragraphes

	%%Mes réglages tcolorbox version 2
	\tcbset{enhanced,colback=orange!10!white,
		boxrule=0.4pt,sharp corners,drop lifted shadow,
		colframe=NavyBlue!75,fonttitle=\bfseries\sffamily}

	\centerline{\Huge{{\textbf{\textcolor{NavyBlue}{\titre}}}}}

	\hrulefill

	\begin{center}
\vspace{\stretch{1}}
		\begin{minipage}{0.65\linewidth}
			\hrulefill
			\renewcommand{\contentsname}{Dans ce chapitre}
			\tableofcontents
%			\vspace{-2mm}
			\hrulefill
		\end{minipage}
	\end{center}
	\thispagestyle{empty}
\vspace{\stretch{3}}
\saut\pagebreak

\pagestyle{monstyle}

\section{Constitution de l'atome}

\subsection{Un premier modèle de l'atome}


\begin{wrapstuff}[type=figure]
	\begin{tikzpicture}[font=\footnotesize]
		%	\filldraw[moncadre] (0,0) rectangle ++ (8,4);

		\coordinate (c) at (3,2);
		\draw (c) circle (1.5cm);
		\draw (c) circle (1.0cm);
		\filldraw (c)++(60:1.5) circle (1pt) --++ (1.5,0)node [right]{électrons};
		\filldraw (c)++(160:1.5) circle (1pt);
		\filldraw (c)++(260:1.5) circle (1pt);
		\filldraw (c)++(140:1.0) circle (1pt);
		\filldraw (c)++(260:1.0) circle (1pt);

		\foreach \n in {1,...,3}
		\filldraw[fill=cyan!50] (c)++(0.2*rand,0.2*rand) circle (0.2cm);
		\foreach \n in {1,...,3}
		\filldraw[fill=orange!50] (c)++(0.2*rand,0.2*rand) circle (0.2cm);
		\draw (c) -- ++ (2,0) node [below right=-1mm,align=left]{\textbf{noyau} :\\ protons\\ neutrons};
		\draw [decorate,decoration={brace}](c)++(3.5,-0.4) --++ (0,-0.65)node[midway,right]{\textbf{nucléons}};
	\end{tikzpicture}
\end{wrapstuff}
L'atome étant invisible, les représentations qu'on en donne sont des \emphase{modèles}, \cad des simplifications (avec leurs défauts et leurs limites) qu'on adapte à l'usage qu'on en fait.

Le modèle ci-contre évoluera au cours du chapitre.

Le \emphase{noyau} central est constitué de \textit{protons} et de \textit{neutrons}, et entouré d'un \emphase{cortège électronique}.

\begin{wrapstuff}
	\begin{tblr}{colspec={rl}}
		\SetCell{c=2}{} Masses	&	\\\hline
		proton	&	\num{1.673e-27} kg	\\
		neutron	&	\num{1.675e-27} kg	\\
		électron	&	\num{9.109e-31} kg	\\\hline
	\end{tblr}
\end{wrapstuff}

En comparant les \emphase{masses} des constituants de l'atome, on constate que :

\opt{prof}{
\begin{itemize}
	\item le proton et le neutron ont \textit{presque la même masse}.
	\item l'électron est plus de 1000 \textit{fois plus léger} que les nucléons.
\end{itemize}
}

\opt{etud}{\hspace{-3cm}
\quadrill{11,1.5}
}

D'autre part, le noyau central est environ \num{100000} fois plus petit que l'atome entier.

\emphase{Dans l'atome, l'essentiel de la masse est concentrée dans le noyau central, de très faible volume.}


\subsection{Symbole complet du noyau}

\compo[0.80]{%

\begin{tcolorbox}[title=Définitions]
	Le \emphase{numéro atomique Z} est le nombre de \textbf{protons} dans le noyau.

	Le \emphase{nombre de masse A} est le nombre de \textbf{nucléons} (protons + neutrons).
\end{tcolorbox}
}{%

\centering
	\begin{tikzpicture}[font=\sffamily]
		\draw (0,0) node (x)[font=\huge]{Cl};
		\draw (x.north west) node[]{35};
		\draw (x.south west) node[]{17};
	\end{tikzpicture}
\captionof{figure}{Le chlore 35 possède 18 neutrons.}
}


\subsection{Isotopes}

\compo[0.60]{

	\begin{tcolorbox}[title=Définition : noyaux isotopes]
		Des noyaux \emphase{isotopes} ont \textit{le même numéro atomique}, mais \textit{un nombre de masse différent}.
	\end{tcolorbox}

\medskip
Des isotopes ont donc un nombre de neutrons différent.
}{

\begin{tblr}{colspec={X X X}}
	\SetRow{font=\Large}
\ce{^24_12Mg}	&	\ce{^25_12Mg}	&	\ce{^26_12Mg}	\\\hline
\num{78.9}\%		&	\num{10.0}\%		&	\num{11.1}\%
\end{tblr}
	\captionof{table}{L'élément chimique magnésium possède 3 isotopes naturels, d'abondance naturelle différente.}
}

La plupart des éléments possèdent plusieurs isotopes naturels ; certains isotopes sont artificiels, souvent radioactifs, et produits notamment pour la médecine nucléaire.

\section{La mole}

\subsection{Nombre d'Avogadro}

\compo[0.48]{

En chimie, on compte les particules (atomes, ions, molécules) par \textit{paquets}, afin de faciliter le comptage.

La \textit{quantité de matière} $ n $ est le nombre de moles dans un échantillon. Unité abrégée : mol.
}{


\begin{tcolorbox}[title=Définition]
	Une \emphase{mole} est un échantillon de \textbf{\num{6.02e23}} particules. Ce nombre s'appelle \textbf{nombre d'\bsc{Avogadro}} et se note $ N_A $.
\end{tcolorbox}
}

\medskip
\compo[0.7]{

Formule reliant $ n $ au nombre d'atomes $ N $ dans l'échantillon :

\textit{Dans \num{2.5} mol d'atomes, il y a :}

\opt{prof}{
$ 2.5\times N_A = 2.5 \times \num{6.02e23} \simeq \num{1.5e24}$ atomes.
}
\opt{etud}{

\dotfill}
}{

\centering
\opt{prof}{
\formenc{N = n \times N_A}
}
\opt{etud}{
\tikz \filldraw[fill=lime!50] (0,0) rectangle ++(3,1);
}
}


\subsection{Masse molaire atomique}

\compo[0.48]{%

Le comptage par \textit{pesée} est très répandu en chimie. Il est efficace parce qu'on sait combien pèse 1 mol d'atomes.

Les valeurs de masse molaire atomique sont données dans le tableau périodique.

}{%

	\begin{tcolorbox}[title=Définition]
		La \emphase{masse molaire atomique} est la masse d'une mole d'atome.

		Symbole : $ M $	\quad unité : \gpm
	\end{tcolorbox}

}

\compo[0.48]{

Les masses molaires atomiques sont souvent très voisines des nombres de masse des atomes. La correspondance n'est pas exacte ; l'une des raisons est l'existence de plusieurs isotopes. Les masses molaires atomiques d'un élément sont les \textit{moyennes} des masses molaires de tous les isotopes naturels de l'élément.

}{

\centering
\input{tableauPerColormap/tabPerClassique}
\captionof{figure}{Extrait du tableau périodique.}
}

\textit{Ex.} $ M_{(\ce{Mg})} = \dfrac{78.9}{100}\times 24 + \dfrac{10.0}{100}\times 25 + \dfrac{11.1}{100}\times 26 \simeq 24.3$ \gpm

\section{Niveaux d'énergie de l'hydrogène}

	\subsection{Spectre de raies}

\begin{wrapstuff}[type=figure,width=0.6\linewidth]
		\begin{tikzpicture}\scriptsize
			\filldraw[moncadre](1.5,-0.5) rectangle ++(10,4);
			\coordinate (source) at (3,2);
			\coordinate (r) at (6,2);
			\draw (source)node[draw,inner sep=8pt,align=center,font=\sffamily\bfseries,rounded corners, fill=OrangeRed,fill opacity=0.2]{hydrogène \\gazeux};
			\draw [thick,->](source) ++ (1,0)--(r);
			\draw (r) ++ (0,1)--++(0,-2)node[below,align=center]{élément \\dispersif};
			\draw [very thick](7,0.5)--++(35:2.5)node[midway,below,sloped,text width=2.5cm]{écran où se projette le spectre};
			\draw (r) -- ++ (-10:2.4);
			\draw (r) -- ++ (-50:1.7);
			\draw (r)++ (3,0.4) node{\pgfspectra[width=4cm,height=0.6cm,element=H]}node[above=4mm]{spectre de l'hydrogène};
		\end{tikzpicture}\normalsize
		\caption{Montage d'observation d'un spectre d'émission}
\end{wrapstuff}
On obtient le \emphase{spectre de raies d'émission} d'un atome en chauffant un échantillon gazeux de l'atome, à basse pression. Après décomposition de la lumière (par un réseau ou un prisme), on observe un spectre de raies colorées sur fond noir.

Ce spectre renseigne sur la structure interne de l'atome d'hydrogène.



\saut\pagebreak

	\subsection{Interprétation : l'atome de Bohr}

\begin{wrapstuff}[type=figure,width=4cm]
	\centering
	\bohr[4]{1}{H}
	\caption{Modèle de \bsc{Bohr} de l'atome d'hydrogène.}
\end{wrapstuff}
Les électrons d'un atome ne sont pas équivalents, et sont plus ou moins fortement liés au noyau. On dit qu'ils se répartissent sur des \emphase{couches électroniques}.

En 1913, le physicien danois Nils \bsc{Bohr} propose un modèle pour rendre compte du spectre de raies de l'hydrogène. L'unique électron de l'hydrogène ne peut se situer que sur certaines orbites autour du noyau ; dans chacune de ces \textit{couches}, l'électron possède une énergie bien précise : on dit que l'énergie de l'électron est \emphase{quantifiée}.

\medskip
\centerline{
	\formenc{E_n = -\dfrac{13.6}{n^2}}\quad (en électronvolts eV)
}

\medskip

\begin{wrapstuff}[type=figure,width=4cm]
	\begin{tikzpicture}
\pgfkeys{/pgf/number format/.cd,fixed,fixed zerofill,precision=2}
%		\filldraw[moncadre] (0,0) rectangle ++ (4,9);
		\begin{scope}[yscale=0.5,yshift=15cm]
		\foreach \n in {1,...,5}
		\pgfmathsetmacro{\En}{-13.6/\n^2}{%
			\draw [thick](1.2,\En)node[left,font=\footnotesize]{\pgfmathprintnumber{\En}} --++ (2,0)node[right,font=\tiny]{$ E_{\n} $};
	\ifnum \n <4 \draw (2.2,\En) node[below=-2pt,font=\footnotesize]{$ n= $ \n};
	\fi
}
		\draw [thick](1.2,0)node[left,font=\footnotesize]{0}--++ (2,0)node[right,font=\tiny]{$ E_{\infty} $};
		\end{scope}
	\draw[thick,-Latex] (1.2,0.5)--++ (0,7.5)node[above]{$ E_{(eV)} $};
	\end{tikzpicture}
	\caption{Diagramme des niveaux d'énergie de l'hydrogène. Les niveaux sont de plus en plus resserrés vers le haut (on ne les représente pas tous).}
\end{wrapstuff}
\vspace{-2cm}
$ n $ s'appelle \emphase{le nombre quantique principal}.
C'est le \textbf{numéro de la couche}, à partir de la plus proche du noyau. $ n $ = 1, 2, 3, 4,\dots~La formule donne les valeurs des \emphase{niveaux d'énergie} de l'atome d'hydrogène.

Si l'électron se trouve sur la couche $ n=1 $, l'atome se trouve à \emphase{l'état fondamental}.

Le niveau le plus haut en énergie est $ E_\infty  = 0$ : c'est \emphase{l'état ionisé} : l'électron a été arraché à l'atome.

On définit \emphase{l'énergie d'ionisation} comme l'énergie nécessaire pour arracher l'électron à partir de l'état fondamental. Cela correspond à la transition $ E_1 \rightarrow E_\infty $ : $ E_i = E_\infty - E_1 = 0 - (-13.6) = 13.6 $~eV.

\begin{figure}[h]
	\includegraphics[width=0.7\linewidth]{images/spectreH_UV_VIS_IR.png}
%	\caption{Le spectre de raies d'émission de l'hydrogène, entre 90 et 2000 nm.}
\end{figure}

\begin{center}

\includegraphics[width=0.5\linewidth]{images/Hydrogen_transitions.png}
\captionof{figure}{\textbf{Interprétation du spectre de raies} : lorsque l'électron passe d'une couche à une autre, plus basse en énergie, l'atome émet un rayonnement. La longueur d'onde du rayonnement émis correspond à celles des raies du spectre. Par exemple, la transition du niveau $ n=4 $ au niveau $ n=2 $ s'accompagne d'une émission lumineuse de longueur d'onde 486~nm : c'est une raie bleue du spectre de l'hydrogène.}
\end{center}

\begin{center}
	\input{spectreHcalcule/spectre_H_Lyman}
\hfill
\input{spectreHcalcule/spectre_H_Balmer}
%\input{spectreHcalcule/spectre_H_Paschen}

\end{center}
%\saut\pagebreak

\section{Atomes polyélectroniques}

	\subsection{Autres nombres quantiques}

Le modèle de \bsc{Bohr} n'est valable que pour l'hydrogène et les ions n'ayant qu'un électron. Pour les atomes polyélectroniques, il faut utiliser des résultats de la physique quantique pour décrire l'organisation du cortège électronique en niveaux d'énergie.

%\begin{center}
\begin{minipage}{0.4\linewidth}

	\begin{tikzpicture}[ultra thick]
%		\filldraw[moncadre] (0,0) rectangle ++ (8,4);
\draw [-Latex] (0,0) --++ (0,8.5)node[above]{$ E $};
	\draw (0.5,1) --++ (1,0)node[midway,below]{$ 1s $};
\begin{scope}[OliveGreen]
	\draw (0.5,2) --++ (1,0)node[midway,below]{$ 2s $};
	\foreach \l in {1,2.2,3.4}
		\draw (-0.5+\l,3) --++ (1,0)node[midway,below]{$ 2p $};
\end{scope}
\begin{scope}[OrangeRed]
	\draw (0.5,4) --++ (1,0)node[midway,below]{$ 3s $};
	\foreach \l in {1,2.2,3.4}
	\draw (-0.5+\l,5) --++ (1,0)node[midway,below]{$ 3p $};
	\foreach \l in {1,2.2,3.4,4.6,5.8}
	\draw (-0.5+\l,7) --++ (1,0)node[midway,below]{$ 3d $};
\end{scope}
\begin{scope}[violet]
	\draw (0.5,6) --++ (1,0)node[midway,below]{$ 4s $};
	\foreach \l in {1,2.2,3.4}
	\draw (-0.5+\l,8) --++ (1,0)node[midway,below]{$ 4p $};
\end{scope}
	\end{tikzpicture}
\end{minipage}
%\end{center}
\hfill
\begin{minipage}{0.5\linewidth}
	\captionof{figure}{\textbf{Niveaux d'énergie des atomes polyélectroniques}.\\Par rapport à l'hydrogène, tous les niveaux (sauf le fondamental) sont séparés en plusieurs niveaux : \textbf{les sous-couches} électroniques.\\ Les sous-couches $ p $ sont en 3 parties, les sous-couches $ d $ en 5 parties (voir ci-dessous).\\ Les électrons se répartissent sur ces niveaux selon les règles de remplissage décrites dans le paragraphe suivant.\\Les nombres maximaux d'électrons par sous-couche sont :\\ $ s $ : \textbf{2} électrons \quad	$ p $ : \textbf{6} électrons\\ $ d $ : \textbf{10} électrons \quad $ f $ : \textbf{14} électrons}
\end{minipage}

On représente également ces niveaux sous forme de \emphase{cases quantiques} (maximum 2 électrons par case):

sous-couche $ s $ : \caseq{0}	\hspace{3.5cm}
sous-couche $ p $ : \caseq{0} \caseq{0} \caseq{0}

sous-couche $ d $ : \caseq{0} \caseq{0} \caseq{0} \caseq{0} \caseq{0}
\qquad
sous-couche $ f $ : \caseq{0} \caseq{0} \caseq{0} \caseq{0} \caseq{0} \caseq{0} \caseq{0}

%\saut\pagebreak

Il faut considérer \emphase{4 nombres quantiques} :

\begin{center}

\begin{tblr}{width=0.8\linewidth,colspec={X[r,3] X[c,1] X[c,2]  X[l,3]},hlines,rows={valign=m}}
	\SetRow{font=\bfseries\sffamily}
	nom	&	symbole	&	valeurs		&	remarque	\\
	nombre quantique principal	&	$ n $	&	1, 2, 3, 4\dots	&	numéro de la couche électronique	\\
	nombre quantique secondaire &	$ l $	&	0, 1, 2,\dots,  $ n-1 $	&	sous-couche $ s, p, d, f $ \\
	nombre quantique magnétique &	$ m_l $	&	$ -l \leq m_l \leq +l$ &	 \\
	nombre quantique de spin 	&	$ m_s $	&	$ +\frac{1}{2} $ ou $-\frac{1}{2} $	&	\\
\end{tblr}
\end{center}

Chaque électron d'un atome est défini par une série de 4 valeurs des nombres quantiques. Deux électrons d'un atome ne peuvent pas avoir leurs 4 nombres quantiques identiques.

\begin{center}
	\begin{tblr}{colspec={X[c,1] X[c,1] X[c,1] X[l,4] X[l,1] X[c,2]},width=\linewidth}\hline
		$ n=1 $	&	$ l=0 $	&	1$ s $	&	&	1 case &	2 électrons	\\\hline
		$ n=2 $	&	$ l=0 $	&	2$ s $	&	&	1 case &2 électrons	\\
				&	$ l=1 $	&	2$ p $	&	$ m_l=-1, 0, +1 $	&	3 cases & 6 électrons	\\\hline
		$ n=3 $	&	$ l=0 $	&	3$ s $	&	&	1 case &2 électrons	\\
				&	$ l=1 $	&	3$ p $	&	$ m_l=-1, 0, +1 $	&	3 cases & 6 électrons	\\
				&	$ l=2 $	&	3$ d $	&	$ m_l=-2, -1, 0, +1, +2 $	&	5 cases & 10 électrons	\\\hline
		$ n=4 $	&	$ l=0 $	&	4$ s $	&	&	1 case & 2 électrons	\\
				&	$ l=1 $	&	4$ p $	&	$ m_l=-1, 0, +1 $	&	3 cases & 6 électrons	\\
				&	$ l=2 $	&	4$ d $	&	$ m_l=-2, -1, 0, +1, +2 $	&	5 cases & 10 électrons	\\
				&	$ l=3 $	&	4$ f $	&	$ m_l=-3, -2, -1, 0, +1, +2, +3 $	&	7 cases & 14 électrons	\\\hline
				etc\dots	\\

	\end{tblr}
\end{center}



	\subsection{Règles de remplissage}

Lorsqu'on dispose les électrons d'un atome sur les différentes couches et sous-couches, les règles suivantes doivent être respectées :

\compo[0.55]{

\begin{itemize}
	\item on place les électrons selon \textbf{l'ordre croissant de l'énergie} des niveaux.
	\item l'ordre des niveaux est donné par \emphase{la règle de \klech} (voir encadré).
	\item règle de \bsc{Hund} : dans une sous-couche, on place les électrons dans des \textbf{cases différentes}, autant que possible.
	\item principe d'exclusion de \bsc{Pauli} : dans une même case, on ne peut placer que \textbf{2 électrons} (de 2 spins différents).
\end{itemize}
}{

\input{schemaKlech.tex}
\captionof{figure}{Règle de \klech}
}


	\subsection{Structure électronique}

Tout ce qui précède a pour but d'être capable d'écrire la structure électronique d'un atome, connaissant son numéro atomique. C'est la répartition de ses électrons sur les couches et sous-couches. On dit aussi \textit{configuration électronique}.

Quelques exemples :

Structure électronique du carbone (Z=6) :
\opt{prof}{
 \elconf{6}\qquad
 \caseq{2} \quad \caseq{2} \quad \caseq{1} \caseq{1} \caseq{0}
}
\opt{etud}{
\dotfill
\caseq{0}~~ \caseq{0} ~~\caseq{0} \caseq{0} \caseq{0}
}

\vspace{0.25cm}
Structure électronique du magnésium (Z=12) :
\opt{prof}{
 \caseq{2} \quad \caseq{2} \quad \caseq{2} \caseq{2} \caseq{2} \quad \caseq{2}
 \elconf{12}\quad
}
\opt{etud}{\dotfill
\caseq{0}~~ \caseq{0} ~~\caseq{0} \caseq{0} \caseq{0}
}

\vspace{0.25cm}
Structure électronique du manganèse (Z=25) :
\opt{prof}{
  1s$ ^2 $ 2s$ ^2 $ 2p$ ^6 $ 3s$ ^2 $ 3p$ ^6 $ 4s$ ^2 $ 3d$ ^5 $

\caseq{2} \quad \caseq{2} \quad \caseq{2} \caseq{2} \caseq{2} \quad \caseq{2}\quad \caseq{2} \caseq{2} \caseq{2}
\quad
\caseq{2}	\quad \caseq{1} \caseq{1} \caseq{1} \caseq{1} \caseq{1}
}
\opt{etud}{\dotfill

\hfill
\caseq{0}~~ \caseq{0} ~~\caseq{0} \caseq{0} \caseq{0}
~~~\caseq{0}~~ \caseq{0} \caseq{0} \caseq{0}~~~ \caseq{0} ~~\caseq{0} \caseq{0}  \caseq{0} \caseq{0} \caseq{0}
}

\vspace{5mm}
Ion sulfure \ce{S^2-} : Z = 16 mais il a \textit{2 électrons supplémentaires} :\opt{prof}{ \elconf{18}}
\opt{etud}{\dotfill}

Ion aluminium \ce{Al^3+} : Z = 13 mais il a \textit{3 électrons en moins} : \opt{prof} {\elconf{10}}
\opt{etud}{\dotfill}

\begin{tcolorbox}
\emphase[red]{Stabilité et \struct.} Un atome est plus stable si \emphase{sa couche externe} est \emphase{saturée}.
\end{tcolorbox}

\medskip
\textit{Exemples} :
\begin{itemize}
\item Ne (
\opt{prof}{\elconf{10}}
\opt{etud}{\ligne{3}}
)
 est plus stable que F (
\opt{prof}{\elconf{9}}
\opt{etud}{\ligne{3}}
).
\item \na (
\opt{prof}{\elconf{10}}
\opt{etud}{\ligne{3}}
)
 est plus stable que Na (
\opt{prof}{\elconf{11}}
\opt{etud}{\ligne{3}}
).
\end{itemize}

\section{Tableau périodique des éléments}


\subsection{Construction du \tabper}


Les éléments sont ordonnés par \textbf{numéro atomique croissant}.

Les éléments d'une \textbf{même colonne} présentent des \textbf{propriétés chimiques similaires}.

La version la plus courante du \tabper dispose de 18 colonnes et 7 lignes.

Les lignes des lanthanides et actinides sont généralement décalées en bas du tableau pour éviter un tableau trop long.

\subsection{Structure du \tabper}

La structure traduit l'ordre de remplissage des sous-couches électroniques donné par la règle de \klech : une nouvelle ligne commence à chaque changement de couche électronique, \cad à chaque nouvelle valeur du \textit{nombre quantique principal n}.

Elle présente une structure en \textit{blocs}, chaque bloc correspondant au remplissage d'une \textit{sous-couche} $ s $, $ p $, $ d $ ou $ f $.


\begin{center}
	\begin{tikzpicture}[yscale=-0.8,xscale=0.8,draw=white]
		\colorlet{blocS}{orange!40}
		\colorlet{blocP}{violet!40}
		\colorlet{blocD}{blue!40}
		\colorlet{blocF}{cyan!40}

		\foreach \n in {1,...,18}
		\draw (\n+0.5,0.5) node [gray]{\scriptsize \sffamily \n};

		\begin{scope}[fill=blocS]
			\filldraw (1,1) rectangle ++(1,1);
			\filldraw (18,1) rectangle ++(1,1);

			\foreach \col in {1,2}
			\foreach \lig in {2,...,7}
			\filldraw (\col,\lig) rectangle ++(1,1);
		\end{scope}

		\begin{scope}[fill=blocP]
			\foreach \col in {13,...,18}
			\foreach \lig in {2,...,7}
			\filldraw (\col,\lig) rectangle ++(1,1);
		\end{scope}

		\begin{scope}[fill=blocD]
			\foreach \col in {3,...,12}
			\foreach \lig in {4,...,7}
			\filldraw (\col,\lig) rectangle ++(1,1);
		\end{scope}

		\begin{scope}[fill=blocF]
			\foreach \col in {3,...,16}
			\foreach \lig in {8.5,...,9.5}
			\filldraw (\col,\lig) rectangle ++(1,1);
		\end{scope}

		% sous-couches sur les cases :

		\foreach \i in {2,...,7}
		\draw (2,\i+0.5) node {\i$ s $};

		\begin{scope}[Latex-Latex,black]
			\draw  (2,1.5) node[left]{$ 1s $} --++ (16,0)node[right]{$ 1s $};


			\foreach \i in {2,...,7}
			\draw (13,\i+0.5) --++ (6,0)node[midway,fill=blocP]{\i$ p $};

			\foreach \i in {3,...,6}
			\draw (3,\i+1.5) --++ (10,0)node[midway,fill=blocD]{\i$ d $};

			\foreach \i in {4,5}
			\draw (3,\i+5) --++ (14,0)node[midway,fill=blocF]{\i$ f $};
		\end{scope}
	\end{tikzpicture}
	\captionof{figure}{Les blocs dans le \tabper. Le nombre de colonnes de chaque bloc correspond au nombre maximal d'électrons dans la sous-couche correspondante : 2, 6, 10 et 14.\\L'hélium ($ 1s^2 $) possède une couche saturée : il est donc placé en dernière colonne.}\label{blocs}
\end{center}

\begin{description}[font=\sffamily]
	\item[Les lignes (ou périodes)]
Chaque ligne correspond à une \textit{couche électronique}. Ainsi, les éléments d'une même ligne ont en commun \textbf{une même couche de valence.}
\item[Les colonnes (ou familles)]
Les éléments d'une même colonne possèdent \textbf{une même dernière sous-couche}, \cad un même état de remplissage de leur couche de valence.

\end{description}

%\saut\pagebreak

%\begin{figure}[h]
%	\begin{center}
%		\input{tableauPerColormap/tabPerConfig.tex}
%\caption{Sous-couche en cours de remplissage. Les éléments d'une même colonne ont la même dernière sous-couche, avec le même nombre d'électrons (avec quelques irrégularités pour certains éléments du bloc d).}
%	\end{center}
%\end{figure}

\subsection{Place d'un élément dans le \tabper}

\subsubsection{Position d'après la structure électronique}

\begin{description}[font=\sffamily,itemsep=4mm]
	\item[Exemple 1 :] le silicium (Z=14) : $ 1s^2~2s^2~2p^6~3s^2~3p^2 $


	Son \textbf{numéro de ligne} est \opt{prof}{\textbf{3}, car \textit{le plus grand nombre quantique principal} est 3.}
\opt{etud}{\dotfill}

	Son \textbf{numéro de colonne} est \opt{prof}{\textbf{14} : il est dans la 2\ieme colonne du bloc $ p $, donc après le bloc $ s $ (2 colonnes) et le bloc $ d $ (10 colonnes) : 2 + 10 + 2 = 14\ieme colonne.}\opt{etud}{\dotfill}

\opt{etud}{\vspace{\baselineskip}
\quadrill{16.5,1.0}}
	\item[Exemple 2 :] le manganèse (Z=25) :
	$ 1s^2~2s^2~2p^6~3s^2~3p^6~4s^2~3d^5 $

	Son \textbf{numéro de ligne} est \dotfill

	Son \textbf{numéro de colonne} est \dotfill
\end{description}

\subsubsection{Structure électronique d'après la position}


Le fer \ce{_26Fe} se trouve dans la 4\ieme ligne et la 8\ieme colonne. Quelle est sa \struct ?

%Il est dans la 6\ieme colonne du bloc $ d $ ; sa \config se termine donc en $ nd^6 $.

%Il a déjà complété la sous-couche $ 4s $. Sa \config se termine donc en $ 4s^2~3d^6 $.

%Sa \config complète est donc : 	$ 1s^2~2s^2~2p^6~3s^2~3p^6~4s^2~3d^6 $
\dotfill

\dotfill

\dotfill


\subsection{Évolution des propriétés dans le \tabper}


Dans cette partie, on choisit successivement une propriété des éléments chimiques (rayon atomique, énergie d'ionisation, affinité électronique, électronégativité), et on observe comment évolue cette propriété dans le \tabper.

\subsubsection{Rayon atomique}

Par définition, le rayon atomique est le rayon de la couche de valence ; il donne une estimation de la taille de l'atome.
Voir \cref{rayon} p.\pageref{rayon}.


\begin{figure}[p]
	\begin{center}
\input{tableauPerColormap/tabPerRayonAtomique.tex}

%		\captionof{figure}{Le rayon atomique augmente vers la gauche et vers le bas.}\label{rayon}
		\caption{Le rayon atomique augmente vers la gauche et vers le bas (irrégularité pour le néon Ne). À chaque nouvelle couche électronique, le rayon atomique augmente, car la nouvelle couche est plus éloignée du noyau.\\
Pour une période (ligne) donnée, le rayon atomique \textbf{diminue quand Z augmente} : en effet, à chaque nouvel électron, la charge du noyau augmente d'une unité, ce qui attire davantage l'ensemble des électrons.}\label{rayon}
	\end{center}
\end{figure}


\begin{figure}[p]

	\begin{center}
\input{tableauPerColormap/tabPerEnergieIonisation.tex}
		\caption{L'énergie d'ionisation \textbf{diminue vers le bas} du \tabper : en effet, pour un remplissage équivalent de la couche externe, l'électron à arracher se trouve plus loin du noyau ; il est donc plus facile à arracher.\\
L'évolution horizontale est moins régulière : on observe une augmentation juste avant le remplissage d'une sous-couche : puisque l'électron à arracher contribue à compléter la sous-couche, il est plus difficile de l'arracher. De la même manière, au début d'une nouvelle couche ou sous-couche, l'électron est facile à arracher puisque sa disparition va stabiliser l'atome.}\label{energieionisation}
	\end{center}
\end{figure}

\subsubsection{Énergie d'ionisation}

Par définition, l'énergie d'ionisation d'un atome est l'énergie qu'il faut fournir pour arracher un électron à l'atome (à l'état gazeux). Voir \cref{energieionisation} p.\pageref{energieionisation}.

%\saut\pagebreak

\subsubsection{Affinité électronique}

Par définition, l'affinité électronique est l'énergie dégagée lorsqu'un atome gazeux capte un électron. Cette grandeur mesure donc le gain en stabilité obtenu par l'acquisition d'un électron (voir \cref{affinite} \cpageref{affinite}).

\begin{figure}[h]
	\begin{center}
\input{tableauPerColormap/tabPerAffiniteElec.tex}
\caption{L'affinité électronique \textbf{augmente vers la droite}. En effet, plus on approche du remplissage d'une couche, plus l'atome gagne en stabilité en acceptant un nouvel électron (stabilité de la couche de valence).\\
D'autre part, le long d'une période, on observe des irrégularités au niveau du remplissage d'une sous-couche. Par exemple, l'affinité électronique du gallium Ga est inférieure à celle du cuivre Cu. Le cuivre est en effet proche du remplissage de la sous-couche $ d $.}\label{affinite}
	\end{center}
\end{figure}

%\newpage

\subsubsection{Électronégativité}

L'électronégativité est une échelle de 0 à 4 définie pour la première fois par le chimiste américain L.~\bsc{Pauling}\footnote{D'autres échelles d'électronégativité ont été définies plus tard.} (voir \cref{electroneg} \cpageref{electroneg}).
Par définition, l'électronégativité d'un atome est sa capacité à \textbf{attirer les électrons d'une liaison covalente}.



\begin{figure}[h!]
	\begin{center}
\input{tableauPerColormap/tabPerElectronegativite.tex}
\caption{L'électronégativité augmente vers la droite et vers le haut.}\label{electroneg}
	\end{center}
\end{figure}









\end{document}