
\section{Le modèle de Lewis}

	\subsection{Formule de Lewis des atomes}

La formule de \lewis d'un atome ne fait apparaître que \emphase{les électrons de la couche de valence}.

\begin{description}
	\item [Les électrons célibataires :] ce sont les électrons se trouvant \textbf{seuls dans une case quantique} de la \struct. Ils sont représentés par des \textbf{points} (\tikz[baseline=-0.5ex]\filldraw (0,0) circle (1.5pt);)
	\item[Les doublets non-liants :] c'est une paire d'électrons se trouvant \textbf{dans la même case quantique}. On la représente par un \textbf{trait} \tikz[baseline=-0.5ex]\draw (0,0) --++ (1em,0);
\end{description}

Pour retrouver la formules de \lewis d'un atome, il faut donc se baser sur sa \struct.


\begin{center}
	\begin{tblr}{%
			width=0.9\linewidth,
			colspec={X[r,1.5] X[l,2] X[c,4]X[c,2] X[c]},
			stretch=1.5
		}\hline
\SetRow{bg=lime!30,valign=m,font=\sffamily\bfseries}
atome	&	\struct	&	remplissage des cases	&	formule de \lewis	&	valence		\\\hline
hydrogène &	\elconf{1}	&	1s \caseq{1} 	&	\opt{prof}{\chemfig{\charge{0=.}{H}}}	&	1\\
oxygène	&	\elconf{8}	&	2s \caseq{2} 2p \caseq{2}\caseq{1}\caseq{1}	&	\opt{prof}{\chemfig{\charge{0=.,90=\|,180=.,270=\|}{O}}}	&	2\\
azote	&	\elconf{7}	&	2s \caseq{2} 2p \caseq{1}\caseq{1}\caseq{1}	&	\opt{prof}{\chemfig{\charge{0=.,90=\|,180=.,270=.}{N}}}	&	3\\\hline
	\end{tblr}
\end{center}

\setchemfig{atom style={scale=1.2}}

\remarque{Remarque.}{
 La disposition des points et des traits autour de la lettre de l'élément chimique n'a pas d'importance : \quad
\chemfig{\charge{270=\.,180=\|,0=\:}{N}}
\qquad	\chemfig{\charge{0=\:,150=\|,210=\|}{O}}
}

\medskip
\begin{cadre0}{}
La \emphase{valence} d'un atome est le \textbf{nombre de liaisons qu'il est capable de former}. Elle correspond au \textbf{nombre d'électrons célibataires} dans la formule de \lewis.
\end{cadre0}

\medskip
Cependant, pour certains atomes, la valence constatée expérimentalement est en désaccord avec la \struct.

Pour le carbone : \elconf{6}\quad 2s \caseq{2} \quad 2p \caseq{1}\caseq{1}\caseq{0}
\quad on devrait avoir
\quad
\chemfig{\charge{0=\.,180=\.,90=\|}{C}} \quad valence 2

En réalité, le carbone a une valence 4 dans les molécules. On l'explique par le passage d'un électron 2s sur la sous-couche 2p :
\setelectrondistribution{6}{2,1+3}
\setchemfig{atom style={scale=1.6}}

\begin{center}
	\begin{tblr}{%
			width=1\linewidth,
			colspec={X[r,1.5] X[l,3] X[c,3.4]X[c,2] X[c]},
			stretch=1.7,
			rowspec={Q[m] Q[m] Q[m] Q[m] Q[m]}
		}\hline
		\SetRow{bg=lime!30,valign=m,font=\sffamily\bfseries}
		atome	&	\struct	&	remplissage des cases	&	\lewis	&	valence		\\\hline
		carbone	&	\elconf{6}	&	2s \caseq{1} 2p \caseq{1}\caseq{1}\caseq{1}	&	\chemfig{\charge{0=\.,90=\.,180=\.,270=\.}{C}}	&	4\\\hline
		soufre	&	\elconf{16}	&	3s \caseq{2} 3p \caseq{2}\caseq{1}\caseq{1} &	\chemfig{\charge{0=\.,90=\|,180=\.,270=\|}{S}}	&	2\\\hline


		soufre	&	\setelectrondistribution{16}{2,2+6,2+3+1}\elconf{16}	&	3s \caseq{2} 3p \caseq{1}\caseq{1}\caseq{1} 3d~\caseq{1}\caseq{0}\caseq{0}\caseq{0}\caseq{0}	&	\chemfig{\charge{0=\.,90=\|,180=\.,270=\:}{S}}	&	4\\\hline
		soufre	&	\setelectrondistribution{16}{2,2+6,1+3+2}\elconf{16}	&	3s \caseq{1} 3p \caseq{1}\caseq{1}\caseq{1} 3d~\caseq{1}\caseq{1}\caseq{0}\caseq{0}\caseq{0}	&	\chemfig{\charge{0=\.,90=\:,180=\.,270=\:}{S}}	&	6\\\hline
	\end{tblr}
\captionof{table}{Exemples d'atomes avec une hypervalence. Le soufre peut adopter les valences 2, 4 ou 6.}\label{tab:hypervalence}
\end{center}
\setchemfig{atom style={scale=1.2}}

%\saut\pagebreak

	\subsection{Formule de \lewis des molécules}

		\subsubsection{Principe de formation de la liaison covalente}

\compo[0.7]{

Deux atomes A et B créent une liaison covalente en mettant en commun un électron célibataire chacun, situé sur leur couche de valence.
}{

\centering
\chemfig[bond style={white}]{@{a}\charge{0=\.}{A} - @{c}{\:}-@{b}\charge{180=\.}{B}}

\chemmove{\draw [-Latex,gray] (a)++(6pt,0) .. controls +(70:5mm)  and +(120:5mm).. (c);}
\chemmove{\draw [-Latex,gray] (b)++(-7pt,0) .. controls +(-110:5mm)  and +(-70:5mm).. (c);}

\chemfig{A - B}
}


		\subsubsection{Règles de stabilité}

Ces règles permettent de prévoir comment vont s'associer les atomes, en devenant plus stables que lorsqu'ils sont isolés.

 \begin{tcolorbox}
\begin{description}[font=\bfseries\sffamily]
	\item[Stabilité des couches saturées.] Un atome a tendance à être plus stable si \textbf{sa couche de valence} est \textbf{saturée}, comme celle du gaz noble le plus proche de lui dans le \tabper.
	\item[Règle du duet.] Les atomes H, Li et Be deviennent plus stables en ayant \textbf{2 électrons} sur leur couche de valence (sous-couche 1s ou 2s saturée).
	\item[Règle de l'octet.] Les autres atomes de la 2\ieme période du \tabper deviennent plus stables en ayant \textbf{8 électrons} sur leur couche de valence (couche 2 saturée).
\end{description}
 \end{tcolorbox}


 		\subsubsection{Quelques exemples}

\setchemfig{atom style={scale=1}}
	\begin{tblr}{colspec={X[c] X[c] X[c]},
		stretch=1.05,
	rowsep=3mm}
	\hline
		\chemfig{\charge{0=\:,120=\|,-120=\|}{O}}
		\quad
		\chemfig{\charge{180=\:,60=\|,-60=\|}{O}}
		&
		\chemfig{\charge{0=\.}{H}}
		\quad
		\chemfig{\charge{180=\.,90=\|,-90=\|,0=\.}{O}}
		\quad
		\chemfig{\charge{180=.}{H}}
		&
		\chemfig{\charge{0=\:,120=\|,-120=\|}{O}}
		\quad
		\chemfig{\charge{0=\:,180=\:}{C}}
		\quad
		\chemfig{\charge{180=\:,60=\|,-60=\|}{O}}
		\\[-1ex]
		\chemfig{\charge{120=\|,-120=\|}{O} = \charge{60=\|,-60=\|}{O}}
		&
		\chemfig{H-\charge{90=\|,-90=\|}{O} - H}
		&
		\chemfig{\charge{120=\|,-120=\|}{O} = C = \charge{60=\|,-60=\|}{O}}
		\\\hline
		\chemfig{H - \charge{90=\|}{N}(-[-3,0.8]H) - H
		}
		&
		\chemfig{H-C(-[3,0.8]H)(-[-3,0.8]H)-\charge{0=\|,90=\|,-90=\|}{Cl}}
		&
		\chemfig{H-C(-[3,0.8]H)(-[-3,0.8]H) - C ~ \charge{90=\|}{N}}
		\\\hline
	\end{tblr}

 	\subsection{Exceptions à la règle de l'octet}

\emphase{Atomes hypervalents.} Le soufre (voir \cref{tab:hypervalence}), mais aussi le phosphore, peuvent avoir une valence supérieure à celle prévue par leur \struct. Ils possèdent alors \textbf{plus de 8 électrons} sur leur couche de valence.

\begin{wrapstuff}[type=figure,width=0.40\linewidth]
\centering
\chemfig{H-\charge{90=\"}{B}(-[-3]H)-H}
\qquad
\chemfig{\charge{180=\|,90=\|,-90=\|}{Cl}-\charge{90=\"}{Al}(-[-3]\charge{0=\|,180=\|,-90=\|}{Cl})-\charge{0=\|,90=\|,-90=\|}{Cl}}
\end{wrapstuff}
\emphase{Les atomes à lacune électronique}.
	Le bore B et l'aluminium Al ont une \struct en $ ns^2~np^1 $, ce qui conduirait à une valence 1. Or on constate qu'ils ont une valence 3 dans les molécules.

	Comme pour le carbone, on l'explique en supposant le passage d'un électron $ ns $ sur la sous-couche $ np $ : \quad
	$ ns^1 $ \caseq{1}
	\qquad $ np^2 $  \caseq{1}\caseq{1}\caseq{0}

Avec une valence 3, B et Al ne peuvent pas suivre la règle de l'octet (6 électrons au lieu de 8). On représente parfois cet écart à la règle par une \textbf{lacune} \tikz \draw (0,0) rectangle ++ (0.4,0.1); qui symbolise la case quantique vide. Des molécules comportant de tels atomes sont appelées des \textbf{acides de \lewis}.

\compo[0.85]{

\emphase{Les radicaux}.
Dans certaines molécules, un atome peut comporter \textbf{un électron célibataire}. L'atome n'est donc pas entouré de 8 électrons. Une telle molécule s'appelle un \textbf{radical}. Les radicaux sont cependant instables.
 }{

 	\centering
 	\chemfig{\charge{180=\.,90=\|}{N}=\charge{45=\|,-45=\|}{O}}
 }

\vspace{\baselineskip}

	\subsection{Les ions polyatomiques}


\emphase{Les anions}
(ions ayant gagné un ou plusieurs électrons) portent une charge \textbf{négative}, et les électrons supplémentaires rejoignent les électrons de valence que possédait l'atome.

\emphase{Les cations} ont perdu un ou plusieurs électrons, parmi leurs électrons de valence. Leur charge est positive.

\setchemfig{atom style={scale=1.2}}

\begin{center}
\captionof{table}{Quelques exemples d'ions polyatomiques en formule de \lewis.}
	\begin{tblr}{colspec={X[c,1] X[c,1] X[c,4]},rowspec={Q[m] Q[m] Q[m] Q[m] Q[m] Q[m]},
			hlines,
			rowsep=15pt,
			width=0.72\linewidth
	}
\SetRow{bg=lime!30,font=\sffamily\bfseries}
atome	&	ion seul		&	exemple d'ion polyatomique	\\
\chemfig{\charge{0=\.,180=\.,90=\|,-90=\|}{O}}
&
\opt{prof}{
\chemfig{\charge{0=\|,90=\|,-90=\|,180=\.,45:5pt=\chmoins}{O}}
}
&
\opt{prof}{
\chemfig{H-\charge{0=\|,90=\|,-90=\|,45:5pt=\chmoins}{O}}
}
\\
\chemfig{\charge{0=\.,180=\.,90=\|,-90=\|}{O}}
&
\opt{prof}{
\chemfig{\charge{0=\.,180=\.,90=\|,-90=\.,45:3pt=\chplus}{O}}
}
&
\opt{prof}{
\chemfig{\charge{180=\|,45:4pt=\chmoins}{C} ~ \charge{90=\|,45:3pt=\chplus}{O}}
}
\\
\chemfig{\charge{0=\.,180=\.,90=\|,-90=\.}{N}}
&
\chemfig{\charge{0=\.,180=\.,90=\.,-90=\.,45:3pt=\chplus}{N}}
&
\chemfig{H-\charge{45:3pt=\chplus}{N} (-[-3]H)(-[3]H) -H}
\\
\chemfig{\charge{0=\.,180=\.,90=\|,-90=\.}{N}}
&
\opt{prof}{
\chemfig{\charge{0=\.,180=\.,90=\|,-90=\|,45:3pt=\chmoins}{N}}
}
&
\opt{prof}{
\chemfig{H - \charge{-90=\|,90=\|,45:3pt=\chmoins}{N} - H}
}
\\
\chemfig{\charge{0=\.,180=\.,90=\.,-90=\.}{C}}
&
\opt{prof}{
\chemfig{\charge{0=\.,180=\.,90=\|,-90=\.,45:3pt=\chmoins}{C}}
}
&
\opt{prof}{
\chemfig{H_3C - \charge{90=\|,45:3pt=\chmoins}{C} (-[-3]CH_3) - CH_3}
}
\\
\chemfig{\charge{0=\.,180=\.,90=\.,-90=\.}{C}}
&
\chemfig{\charge{0=\.,180=\.,-90=\.,45:3pt=\chplus}{C}}
&
\chemfig{H_3C - \charge{45:4pt=\chplus}{C} (-[-3]CH_3) - CH_3}
\\
	\end{tblr}
\end{center}

% \saut\pagebreak
\opt{etud}{\saut\pagebreak}

	\section{Mésomérie}

\subsection{Exemple}

Voici 3 écritures de \lewis pour l'ion carbonate.

\setchemfig{atom style={scale=1}}

\begin{center}

\begin{minipage}[m]{0.3\linewidth}
	\centering
\chemfig{\charge{120=\|,-120=\|}{O} = C  (-[-3]\charge{-90=\|,180=\|, 0=\|,-45:4pt=\chmoins}{O})  (- \charge{90=\|,-90=\|,0=\|,45:4pt=\chmoins}{O})}
\end{minipage}
\hfill
\begin{minipage}[m]{0.3\linewidth}
		\centering
	\chemfig{\charge{90=\|,-90=\|,180=\|,45:4pt=\chmoins}{O} - C  (-[-3]\charge{-90=\|,180=\|, 0=\|,-45:4pt=\chmoins}{O})  (= \charge{60=\|,-60=\|}{O})}
\end{minipage}
\hfill
\begin{minipage}[m]{0.3\linewidth}
		\centering
	\chemfig{\charge{90=\|,-90=\|,180=\|,45:4pt=\chmoins}{O} - C  (=[-3]\charge{-120=\|,-60=\|}{O})  (- \charge{90=\|,-90=\|,0=\|,45:4pt=\chmoins}{O})}
\end{minipage}
\end{center}

\bigskip

Toutes respectent les règles vues précédemment ; pourtant aucune ne traduit la réalité. En effet, selon ces formules, les 3 liaisons carbone - oxygène devraient être de longueurs différentes (liaison \chemfig{C-O} plus longue que la liaison \chemfig{C=[,0.8]O}). Or les mesures indiquent que les 3 liaisons sont de même longueur, avec une valeur intermédiaire entre \chemfig{C-O} et \chemfig{C=[,0.8]O}.

\begin{center}
	\begin{tblr}{colspec={XXX[2]},
			width=0.5\linewidth,
		hlines,columns={c},
	}
		\chemfig{C-O}	&	\chemfig{C=[,0.8]O}	&	dans l'ion carbonate	\\
		143	pm &	116 pm &	129 pm	\\
	\end{tblr}
\end{center}

\subsection{Définition}

L'ion carbonate \ce{CO3^2-} est représenté par ces 3 formules à la fois, en passant de l'une à l'autre par \emphase{des mouvements d'électrons} :

\begin{center}
\begin{figure}[h]
\begin{center}
\schemestart
	\chemfig{@{o1}{\charge{120=\|,-120=\|}{O}} =_[@{dl}] C  (-[-3]\charge{-90=\|,180=\|, 0=\|,-45:4pt=\chmoins}{O})  (-[@{ls}] @{o2}{\charge{90=\|,-90=\|,0=\|,45:4pt=\chmoins}{O}})}
\chemmove{\draw[mesomerie] (dl) .. controls +(90:5mm) and +(60:5mm).. (o1);}
\chemmove{\draw[mesomerie] (o2)++(0,0.2) .. controls +(90:5mm) and +(90:5mm).. (ls);}
\arrow{<->}
	\chemfig{\charge{90=\|,-90=\|,180=\|,45:4pt=\chmoins}{O} - C  (-[@{ls}-3]@{obas}{\charge{-90=\|,180=\|, 0=\|,-45:4pt=\chmoins}{O}})  (=_[@{ld}] @{odroite}{\charge{60=\|,-60=\|}{O}})}
\chemmove{\draw[mesomerie] (ld) .. controls +(90:5mm) and +(120:5mm).. (odroite);}
\chemmove{\draw[mesomerie] (obas)++(0.2,0) .. controls +(0:5mm) and +(0:5mm).. (ls);}
\arrow{<->}
	\chemfig{\charge{90=\|,-90=\|,180=\|,45:4pt=\chmoins}{O} - C  (=[-3]\charge{-120=\|,-60=\|}{O})  (- \charge{90=\|,-90=\|,0=\|,45:4pt=\chmoins}{O})}
\schemestop

\bigskip
\caption{\emphase{Les trois formes mésomères de l'ion carbonate}. On passe de l'une à l'autre par \textbf{déplacement de doublets d'électrons}, ces doublets devenant alternativement liants ou non-liants.}
\end{center}
\end{figure}
\end{center}

\begin{tcolorbox}[title=Mésomérie]
Lorsque la structure d'une espèce chimique est représentée par \textbf{plusieurs formules} de \lewis, on parle de \emphase[red]{mésomérie}. Les formules sont qualifiées de \emphase{formes mésomères}.

Le passage d'une forme mésomère à une autre se fait par \textbf{déplacement d'un ou plusieurs doublets d'électrons}. Ces doublets sont dits \textbf{délocalisés}.

Par convention d'écriture, on écrit cette \textbf{double flèche} \ce{<->} entre deux formes mésomères.
\end{tcolorbox}

\medskip

Pour qu'un déplacement d'électrons soit possible, il faut que la formule présente :

\begin{itemize}
\item soit une alternance \textbf{doublet non-liant -- liaison simple -- liaison double} :

\begin{center}
\schemestart
 \chemfig{@{A}{\charge{90=\|}{A}} -[@{l1}] B =[@{l2}] @{C}{C} }
\chemmove{
\draw[mesomerie] (A)++(0,0.2) to [out=60,in=135] (l1);
\draw[mesomerie] (l2)++(0,0.1) to [out=60,in=135] (C.north);
}% fin chemmove
\arrow{<->}
 \chemfig{@{A}{\charge{90:4pt=\chplus}{A}} =[@{l1}] B -[@{l2}] \charge{60:4pt=\chmoins,90=\|}{C} }
\schemestop
\end{center}

\item soit une alternance \textbf{liaison double -- liaison simple -- liaison double} :

\begin{center}
\schemestart
 \chemfig{@{A}{A} =[@{l1}] B -[@{l2}] @{C}{C} =[@{l3}] @{D}{D}}
\chemmove{
\draw[mesomerie] (l1)++(0,0.2) to [out=60,in=135] (l2);
\draw[mesomerie] (l3)++(0,0.1) to [out=60,in=135] (D.north);
}% fin chemmove
\arrow{<->}
 \chemfig{@{A}{\charge{60:4pt=\chplus}{A}} -[@{l1}] B =[@{l2}] @{C}{C} -[@{l3}] @{D}{\charge{60:4pt=\chmoins,90=\|}{D}}}
\schemestop
\end{center}
\end{itemize}


Un exemple courant :\qquad
\schemestart
	\chemfig{
			*6(=[@{l1}]-[@{l2}]=[@{l3}]-[@{l4}]=[@{l5}]-[@{l6}])
			}
	\arrow{<->}
	\chemfig{
			*6(-=-=-=)
			}
\schemestop
\chemmove[mesomerie]{\draw (l1) to [bend left] (l2);}
\chemmove[mesomerie]{\draw (l3) to [bend left] (l4);}
\chemmove[mesomerie]{\draw (l5) to [bend left] (l6);}
\hfill
parfois noté : \chemfig{**6(------)}

\saut\pagebreak

\subsection{Formes mésomères les plus stables}

On peut parfois écrire des formes mésomères qui s'avèrent en réalité peu probables, par manque de stabilité.
Voici quelques critères pour évaluer la stabilité d'une forme mésomère :

\begin{tcolorbox}[title=Critères de stabilité des formes mésomères]

\begin{enumerate}
\item vérifie la règle de l'octet.
\item contient le moins de charges formelles, et les plus éloignées possibles.
\item les charges formelles respectent l'électronégativité (charge négative sur l'atome le plus électronégatif).
\end{enumerate}
\end{tcolorbox}

\medskip
\textit{Exemples} :

\begin{itemize}[itemsep=1cm]
\item molécule \ce{CO} (5 doublets): \qquad
\schemestart
\chemname{
	\chemfig{%
	\charge{180=\|}{C} =_[@{ld}]
	@{oxy}{\charge{60=\|,-60=\|}{O}}
			}
		}{\cancel{octet}}
\chemmove[mesomerie]{%
	\draw (oxy)++(60:2mm) .. controls +(60:5mm) and +(90:5mm) .. (ld);
}
\arrow{<->}
\chemname{%
		\chemfig{%
		\charge{180=\|,90:5pt=\chmoins}{C} ~ \charge{0=\|,60:4pt=\chplus}{O}
		}
		}{octet}
\schemestop
\hfill
forme la plus probable.

\item HCOOH : \qquad
\schemestart
\chemname{
\chemfig{
		H - C(=[@{dl}3] @{oxy}{\charge{60=\|,120=\|}{O}}) (-[@{ls}] @{o2}{\charge{-90=\|,90=\|}{O}} - H)
		}
}{plus probable}
\chemmove[mesomerie]{ \draw (dl)++(1mm,0) .. controls +(0:5mm) and +(-30:5mm) .. (oxy);}
\chemmove[mesomerie]{ \draw (o2)++(0,2mm) .. controls +(90:5mm) and +(90:5mm) .. (ls);}
\arrow{<->}
\chemname{
\chemfig{
		H - C(-[3] @{oxy}{\charge{180=\|,90=\|,0=\|,45:5pt=\chmoins}{O}}) (=[@{ls}] @{o2}{\charge{-90=\|,90:4pt=\chplus}{O}} - H)
		}
}{moins probable}
\schemestop
\hfill
(charges formelles)

\item
ion \ce{CH2CHO^-} : \qquad
\schemestart
\chemname{
\chemfig{
H-[1]@{c}{C}(-[5]H) =_[@{dl}] C (-[-1]H) (-[@{ls}1]@{o}{\charge{-60=\|,30=\|,120=\|,30:5pt=\chmoins}{O}})
}
}{plus probable}
\chemmove[mesomerie]{\draw (dl) .. controls +(90:3mm) and +(45:3mm) .. (c);}
\chemmove[mesomerie]{\draw (o)++(120:2mm) .. controls +(120:5mm) and +(120:5mm) .. (ls);}
\arrow{<->}
\chemname{
\chemfig{
H-[1] \charge{-90=\|,90:4pt=\chmoins}{C}(-[5]H) -[@{dl}] C (-[-1]H) (=_[@{ls}1]@{o}{\charge{-20=\|,120=\|,120=\|}{O}})
}
}{moins probable}
\schemestop

\medskip
L'oxygène étant plus électronégatif que le carbone, la forme de gauche est la plus probable.
\end{itemize}


\section{Géométrie des molécules : théorie VSEPR}

	\subsection{Les hypothèses de VSEPR}

Le chimiste \bsc{Gillespie} a établi en 1957 les hypothèses de la théorie VSEPR (Valence Shell Electron Pair Repulsion, soit \ofg{Répulsion des doublets d'électrons sur la couche de valence}).

\begin{center}
	\begin{tcolorbox}[title=Les hypothèses de la théorie VSEPR]
		\begin{itemize}
			\item On sélectionne un atome de la molécule jouant le rôle d'\textbf{atome central}. L'orientation des liaisons partant de cet atome indiquera la géométrie de la molécule.
			\item L'orientation des liaisons autour de l'atome central est le résultat de la \textbf{répulsion entre les électrons des doublets non-liants}.
			\item Les doublets se repoussent de telle sorte que \textbf{les angles entre les liaisons soient maximaux}.
			\item Les \textbf{liaisons doubles ou triples} sont comptabilisées comme des liaisons simples.
		\end{itemize}
	\end{tcolorbox}
\end{center}